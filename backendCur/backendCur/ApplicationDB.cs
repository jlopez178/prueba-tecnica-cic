﻿
using backendCur.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backendCur
{
    public class ApplicationDB : DbContext
    {
        public DbSet<CursosType> cursost { get; set; }
        public DbSet<DocenteType> docentest { get; set; }
        public DbSet<TemasType> temast { get; set; }
        public ApplicationDB(DbContextOptions<ApplicationDB> options) : base(options)
        {

        }



    }
}
