﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using backendCur.Models;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace backendCur.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TController : ControllerBase
    {
        private readonly ApplicationDB _context;
        public TController(ApplicationDB context)
        {
            _context = context;
        }
        // GET: api/<TController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                List<CursosType> listCursos = await _context.cursost.ToListAsync();

                //using(var ctx = await new ApplicationDB())
                //{
                List<CursoDocenteType> listCursos1 = await (from cursos in _context.cursost 
                                join docente in _context.docentest on cursos.id_docente equals docente.id
                                where cursos.id_estado == 1 select new CursoDocenteType 
                                {
                                    id = cursos.id,
                                    nombre_curso = cursos.nombre_curso,
                                    id_docente = cursos.id_docente,
                                    descrip = cursos.descrip,
                                    id_estado = cursos.id_estado,
                                    n_estudiantes = cursos.n_estudiantes,
                                    estado_doc = docente.estado_doc,
                                    nombre_doc = docente.nombre_doc
                                }).ToListAsync();
                 var query = listCursos1[0].id;
                //}


                return Ok(listCursos1);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET api/<TController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                List<TemasType> listCursos2 = await _context.temast.Where(temast => temast.id_curso == id).ToListAsync();
                
                return Ok(listCursos2);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST api/<TController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CursosType Cursos)
        {
            try
            {
                var name = Cursos;
                 _context.Add(Cursos);
                 await _context.SaveChangesAsync();
                return Ok();
            }
            catch(Exception ex)
            {
                return (BadRequest(ex.Message));
            }
        }
        [HttpPost("temas")]
        public async Task<IActionResult> Post(int id,[FromBody] TemasType tema)
        {
            try
            {
                _context.Add(tema);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return (BadRequest(ex.Message));
            }
        }
        [HttpPost("docentes")]
        public async Task<IActionResult> Post([FromBody] DocenteType docente)
        {
            try
            {
                _context.Add(docente);
                await _context.SaveChangesAsync();
                var LastRecord = (from docentes in _context.docentest
                                 orderby docentes.id descending
                                 select docentes.id).First();
                return Ok(LastRecord);
            }
            catch (Exception ex)
            {
                return (BadRequest(ex.Message));
            }
        }

        // PUT api/<TController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] CursosType Cursos)
        {
            try
            {
                if(id != Cursos.id)
                {
                    return NotFound();
                }
                else
                {
                    _context.Update(Cursos);
                    await _context.SaveChangesAsync();
                    return Ok(new { Message = "Curso actualizado" });
                }
               
            }
            catch (Exception ex)
            {
                return (BadRequest(ex.Message));
            }
        }

        // DELETE api/<TController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var curso = await _context.cursost.FindAsync(id);
            try
            {
                if (curso == null)
                {
                    return NotFound();
                }
                else
                {
                    _context.cursost.Remove(curso);
                    await _context.SaveChangesAsync();
                    return Ok(new { Message = "Curso eliminado" });
                }

            }
            catch (Exception ex)
            {
                return (BadRequest(ex.Message));
            }
        }
    }
}
