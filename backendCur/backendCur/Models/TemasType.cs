﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backendCur.Models
{
    public class TemasType
    {
        
        [Required]
        public int id { get; set; }
        [StringLength(255)]
        [Required]
        public string nombre_temas { get; set; }
        [Required]
        public int id_curso { get; set; }
        [Required]
        public int estado { get; set; }
    }
}
