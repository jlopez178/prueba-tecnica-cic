﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace backendCur.Models
{
    public class CursoDocenteType
    {
        public CursoDocenteType()
        { }
        public int id { get; set; }
        [StringLength(255)]
        [Required]
        public string nombre_curso { get; set; }
        [Required]
        public int id_docente { get; set; }
        [Required]
        [StringLength(255)]
        public string descrip { get; set; }
        [Required]
        public int id_estado { get; set; }
        [Required]
        public int n_estudiantes { get; set; }
        public int estado_doc { get; set; }
        public string nombre_doc { get; set; }
    }
}
