﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace backendCur.Models
{
    public class DocenteType
    {   
        
        [Required]
        public int id { get; set; }
        [StringLength(255)]
        [Required]
        public string nombre_doc { get; set; }
        [Required]
        public int estado_doc { get; set; }
    }
}
