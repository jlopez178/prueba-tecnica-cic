import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CursosService } from 'src/app/services/cursos.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})

export class CursosComponent implements OnInit {
  list_curso_docente: any[] = []


  form: FormGroup;
  constructor(private fb: FormBuilder,private toastr: ToastrService, private _cursos: CursosService) {
    this.form = this.fb.group({
      nombre_curso: ['',Validators.required],
      descrip: ['',Validators.required],
      n_estudiantes: ['',Validators.required],
      nombre_doc: ['',Validators.required],
      nombre_temas:['',Validators.required]
    });

  }

  ngOnInit(): void {
    this.getcursos();


  }
  agregarcurso(){
    
    const docente:any={
      nombre_doc: this.form.get('nombre_doc')?.value,
      estado_doc:1,
    }
    this._cursos.savelistdoc(docente).subscribe(datadoc => {
      console.log(datadoc);
      this.toastr.success('Curso Agregado correctamente!', 'curso agregado!');
      const curso:any={
        nombre_curso: this.form.get('nombre_curso')?.value,
        id_docente:datadoc,
        descrip: this.form.get('descrip')?.value,
        id_estado:1,
        n_estudiantes: this.form.get('n_estudiantes')?.value
      }
      this._cursos.savelistcursos(curso).subscribe(data => {
      this.getcursos();
      console.log(this.form);
      this.toastr.success('Curso Agregado correctamente!', 'curso agregado!');
      this.form.reset();
    }, error => {
      console.log(error)
    })
    }, error => {
      console.log(error)
    })
    
  }
  

  getcursos() {
    this._cursos.getlistcursos().subscribe(data => {
      this.list_curso_docente = data;
      console.log(data)
    }, error => {
      console.log(error)
    })
  }

  deletecursos(id: number) {
    this._cursos.deletelistcursos(id).subscribe(data => {
    this.toastr.error('Curso eliminado correctamente!', 'curso eliminado!') 
    this.getcursos();
    }, error => {
      console.log(error)
    })
  }
}
