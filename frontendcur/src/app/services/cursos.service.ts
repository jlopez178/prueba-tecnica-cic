import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CursosService {
 private myappurl= "https://localhost:44352/api/T/"
 private myappurl_docente= "https://localhost:44352/api/T/docentes"
 private myappurl_curso= "https://localhost:44352/api/T/"
  constructor(private http: HttpClient) { }
  getlistcursos(): Observable<any> {
    return this.http.get(this.myappurl);
  }
  deletelistcursos(id: number):Observable<any>{
    return this.http.delete(this.myappurl+id)
  }
  savelistdoc(docente: any):Observable<any>{
    console.log(docente);
    return this.http.post(this.myappurl_docente,docente)
  }
  savelistcursos(curso: any):Observable<any>{
    
    return this.http.post(this.myappurl_curso,curso)
  }
}
